This repo is mainly a testbed for the MPU6050 and MPU9250 inertial sensors.
If you need to prototype or test these sensors without having to deal with
the integration of libraries in a C/C++ environment, then you might find
these scripts useful.

You may also find this useful if you're looking for sanity checks to make
sure your sensors are outputting the same thing in either C++ or Python.