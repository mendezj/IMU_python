#!/usr/bin/python

# power management registers
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
mpu_addr     = 0x68
data_start_addr = 0x3b
num_bytes_read = 14

ax = ay = az = temp = gx = gy = gz = data = 0

# read a block of data from the i2c bus
# the address is the i2c address, the offset is which register
# youd like to read from, and num_of_bytes is the number of bytes to read
def read_bytes(addr, offset, num_bytes):
	return bus.read_i2c_block_data(addr, offset, num_bytes)

# convert from twos complement
def twos_complement():
	if(val >= 8000):
		return -((65535 - val) + 1)
	else:
		return val

def create_word(hi, lo, pos):
	hi = data[pos]
	lo = data[pos+1]
	return (hi << 8) + lo

def fill_data(array):
	twos_complement	
	
bus = smbus.SMBus(0)

# wake up the imu from sleep mode
bus.write_byte_data(mpu_addr, power_mgmt_1, 0)

while True:
	data = read_bytes(mpu_addr, data_start_addr, num_bytes_read)
	ax = (data[0] << 8) + data[1]
	ay = (data[2] << 8) + data[3]
	az = (data[4] << 8) + data[5]
	temp = (data[6] << 8) + data[7]
	gx = (data[8] << 8) + data[9]
	gy = (data[10] << 8) + data[11]
	gz = (data[12] << 8) + data[13]

	print ax, "\t", ay, "\t", az
	print gx, "\t", gy, "\t", gz	
	sleep(1)

